# CPP Build Container

A Docker image with all tools required for compiling and testing C/C++ code.
This includes latest versions of GCC and Clang, gcov/rcov, Clang Tidy etc.

When programming C/C++ it makes sense to compile and test your programs with different compilers on diferent operating systems and distros to catch all bugs and achieve portability.
For example: Alpine Linux uses Musl where as other distros are using glibc.
This can cause different program behaviour.
Therefore I compile and test my programs with Clang and GCC and use different OSs.

## Building the image

To build locally and deploy the image into the GitLab registry type:

```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/<your name>/cpp-build-container .
docker push registry.gitlab.com/<your name>/cpp-build-container
```

For me this would be

```
docker login registry.gitlab.com
docker build -f Dockerfile.ubuntu -t registry.gitlab.com/hzahnlei/cpp-build-container/ubuntu .
docker push registry.gitlab.com/hzahnlei/cpp-build-container/ubuntu
```

For your convenience I added a `Makefile`:

*  `make alpine` - Builds the Alpine-based C/C++ build image locally.
*  `make fedora` - Builds the Fedora-based C/C++ build image locally.
*  `make ubuntu` - Builds the Ubuntu-based C/C++ build image locally.
*  `make build_and_push` - Builds all images and pushes them to the registry. Meant for CI-Builds. Only invoked on tags.

## Using with GitLab-CI

Start your `.gitlab-ci.yml` with the following line

```
image: registry.gitlab.com/<your name>/cpp-build-container/ubuntu:latest
```

For me this would be

```
image: registry.gitlab.com/hzahnlei/cpp-build-container/ubuntu:latest
```

## Using locally

You can run this image locally (if you have built it locally) by issuing the following command:

```
docker run -it <image> bash
```

Then, inside the bash console you can clone C/C++ projects and compile them, given they
are based on `make` or `cmake`.

But, you do not have to build the image locally.
You can pull it from the GitLab registry.
In the case of my registry this looks like so:

```
docker run -it registry.gitlab.com/hzahnlei/cpp-build-container/fedora20.10:1.0.0 bash
```

## Docker on macOS

For building containers locally (and deploy to GitLab repository), I had to install Docker on my Mac.
I am using Homebrew:
`brew cask install docker`

Then, in the programs folder, double-click on the Docker icon.
This will set the path for command line usage and will also start the Docker daemon.

When prompted for credentials on `docker login ...` I had to use my GitLab credentials, not the passphrase for the SSH key.

## Release Workflow

My release workflow is not fully automated yet.
I am performing the following steps:

1.  Create a new feature branch.
1.  On that branch, add new features to the Docker files or upgrade version of libraries and base images used.
1.  Document in the change log.
1.  Build containers locally (see above) and see if they start etc.
1.  Commit and push.
1.  Tag with a.b.c-RCd.
1.  If projects that use the new container can be build successfully with this new images, then
    1.  merge to master and (merge request)
    1.  tag master with a.b.c.
    1.  deleter feature branch.