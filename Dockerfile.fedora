###############################################################################
#
# C++ Build Container, using Fedora Linux, GCC and Clang
#
# Holger Zahnleiter, 2020-01-26
#
###############################################################################

ARG FEDORA_BASE_IMAGE_TAG
ARG CUSTOM_FEDORA_IMAGE_TAG

FROM fedora:$FEDORA_BASE_IMAGE_TAG

LABEL maintainer="Holger Zahnleiter <opensource@holger.zahnleiter.org>" \
    org.label-schema.vendor="Holger Zahnleiter" \
    org.label-schema.name="Fedora Linux image for building C/C++ projects in CI/CD environments" \
    org.label-schema.license="MIT" \
    org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.version="${FEDORA_BASE_IMAGE_TAG}:${CUSTOM_FEDORA_IMAGE_TAG}" \
    org.label-schema.schema-version="1.0.0-rc.1" \
    org.label-schema.vcs-ref=$VCS_REF \
    org.label-schema.vcs-type="git" \
    org.label-schema.vcs-url="https://gitlab.com/hzahnlei/cpp-build-container"

SHELL ["/bin/bash", "-c"]

# Basic C/C++ build tools
RUN dnf update -y
RUN dnf install -y git make cmake gcc-c++ lcov gcovr valgrind cppcheck
RUN dnf install -y git autoconf libtool pkgconf
RUN dnf install -y clang clang-devel clang-tools-extra clang-analyzer
RUN dnf install -y gtest gtest-devel gmock gmock-devel

# Use Python/Behave for behaviour driven (integration) testing
RUN dnf install -y python3-pip && \
    pip3 install behave

# Conan C/C++ package manager
RUN pip3 install conan==2.0.10 && \
    conan profile detect --name default --force

# An important Conan package registry (besides Conan Central)
RUN conan remote add bincrafters https://bincrafters.jfrog.io/artifactory/api/conan/public-conan

# Catch2 installation so that find_package will find it
# No package available, hence, build from source
RUN git clone https://github.com/catchorg/Catch2.git && \
    cd Catch2 && \
    git checkout v3.4.0 && \
    cmake -Bbuild -H. -DBUILD_TESTING=OFF && \
    cmake --build build/ --target install && \
    cd .. && \
    rm -rf Catch2

RUN dnf install -y doxygen
