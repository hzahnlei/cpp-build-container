.PHONY: alpine fedora ubuntu push

.EXPORT_ALL_VARIABLES:
REGISTRY := registry.gitlab.com/$(or $(CI_PROJECT_NAMESPACE),hzahnlei)/$(or $(CI_PROJECT_NAME),cpp-build-container)

ALPINE_BASE_IMAGE_TAG := 3.18.4
CUSTOM_ALPINE_IMAGE_NAME := "alpine$(ALPINE_BASE_IMAGE_TAG)"
CUSTOM_ALPINE_IMAGE_TAG := 1.0.2
CUSTOM_ALPINE_IMAGE_FULL_NAME := "$(REGISTRY)/$(CUSTOM_ALPINE_IMAGE_NAME):$(CUSTOM_ALPINE_IMAGE_TAG)"

FEDORA_BASE_IMAGE_TAG := 38
CUSTOM_FEDORA_IMAGE_NAME := "fedora$(FEDORA_BASE_IMAGE_TAG)"
CUSTOM_FEDORA_IMAGE_TAG := 1.0.2
CUSTOM_FEDORA_IMAGE_FULL_NAME := "$(REGISTRY)/$(CUSTOM_FEDORA_IMAGE_NAME):$(CUSTOM_FEDORA_IMAGE_TAG)"

UBUNTU_BASE_IMAGE_TAG := 23.10
CUSTOM_UBUNTU_IMAGE_NAME := "ubuntu$(UBUNTU_BASE_IMAGE_TAG)"
CUSTOM_UBUNTU_IMAGE_TAG := 1.0.2
CUSTOM_UBUNTU_IMAGE_FULL_NAME := "$(REGISTRY)/$(CUSTOM_UBUNTU_IMAGE_NAME):$(CUSTOM_UBUNTU_IMAGE_TAG)"

alpine:
	docker build -f Dockerfile.alpine \
	             --build-arg ALPINE_BASE_IMAGE_TAG \
	             --build-arg CUSTOM_ALPINE_IMAGE_TAG \
	             -t $(CUSTOM_ALPINE_IMAGE_FULL_NAME) .
	
fedora:
	docker build -f Dockerfile.fedora \
	             --build-arg FEDORA_BASE_IMAGE_TAG \
	             --build-arg CUSTOM_FEDORA_IMAGE_TAG \
	             -t $(CUSTOM_FEDORA_IMAGE_FULL_NAME) .
	
ubuntu:
	docker build -f Dockerfile.ubuntu \
	             --build-arg UBUNTU_BASE_IMAGE_TAG \
	             --build-arg CUSTOM_UBUNTU_IMAGE_TAG \
	             -t $(CUSTOM_UBUNTU_IMAGE_FULL_NAME) .
	
push: alpine fedora ubuntu
	echo "${CI_REGISTRY_PASSWORD}" | docker login --username "${CI_REGISTRY_USER}" --password-stdin registry.gitlab.com
	docker push $(CUSTOM_ALPINE_IMAGE_FULL_NAME)
	docker push $(CUSTOM_FEDORA_IMAGE_FULL_NAME)
	docker push $(CUSTOM_UBUNTU_IMAGE_FULL_NAME)
