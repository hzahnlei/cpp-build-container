# Changelog

## 1.6.2 (2023-10-16)

*  Adding Bincrafters package registry in addition to Conan Central 
*  Installing gcovr

## 1.6.1 (2023-10-16)

Thought I could omit the "local" Catch2 install. This was wrong. So, now installing
Catch2 again. (Latest version, of course.)

## 1.6.0 (2023-10-16)

*  Main effort: **Upgrading from Conan 1.x to Conan 2.x**.
   As a side-effect I will not be able to use my GitLab package registry any more
   as this does not support revisions yet as required by Conan 2.0.
*  Also updating all images to the latest OS version (also Catch, GCC, etc.).

## 1.5.0

*  Added Doxygen for generating C++ code documentation.

## 1.4.0

*  Updating all images to the latest OS version.
    *   Ubuntu 22.04
    *   Fedora 37
    *   Alpine 3.17.3
*  Upgrading Catch2 from 2.x to 3.3.2
*  Stick with Conan 1.59.0 for now (not 2.0 this time, has breaking changes)

## 1.3.1

*  Removing retired Conan registry on Bintray

## 1.3.0

*  Updating all images to the latest OS version.
    *   Ubuntu 21.10
    *   Fedora 35
    *   Alpine 3.15.0
*  Adding Catch2 (2.13.8) so that CMake's `find_package` will be able to lokate it.
   With CMake Catch2 integration, we can auto-discover tests and produce JUnit-like test reports.

## 1.2.0

*  Updating all images to the latest OS version.
    *   Ubuntu 20.10
    *   Fedora 33
    *   Alpine 3.12.3
*  Changing the naming scheme to include the OS version.
*  Harmonizing the images, they should now contain the same tools and libraries.

## 1.1.0

*  Ubuntu and Fedora images now contain Conan package manager.

## 1.0.1

*  Added Fedora 31 image.

## 1.0.0

*  Ubuntu 18.04 image with GCC, see tag desciption.